const fs = require('fs')
const path = require('path')
const dominant = require('dominant-color')
const request = require('request-promise-native')

const tempDir = path.join(__dirname, '../tmp')
if (!fs.existsSync(tempDir)) fs.mkdirSync(tempDir)

module.exports = async ({ link }) => {
  if (!link) {
    console.error('`link` is required.')
    return
  }

  const image = `http://www.anime1.com/main/img/content/${link}/${link}-210.jpg`
  const imageName = `${link}-cover${path.extname(image)}`
  const tempImage = path.join(tempDir, imageName)
  const tempStream = fs.createWriteStream(tempImage)

  const { statusCode } = await request({
    method: 'HEAD',
    url: image,
    resolveWithFullResponse: true
  })

  if (statusCode !== 200) {
    return new Error('Image does not exist.')
  }

  await new Promise(async (resolve, reject) => {
    await request.get(image).pipe(tempStream)

    tempStream.on('finish', resolve)
    tempStream.on('error', reject)
  })

  const colour = await new Promise((resolve, reject) => {
    dominant(tempImage, (error, col) => {
      if (error) return reject(error)

      resolve(parseInt(col, 16))
    })
  })

  fs.unlinkSync(tempImage)

  return colour
}
