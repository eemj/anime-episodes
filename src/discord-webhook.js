const request = require('request-promise-native')
const { StatusCodeError } = require('request-promise-native/errors')
const moment = require('moment')
const path = require('path')

const webhooksDir = path.resolve(__dirname, '../webhooks.json')
const WEBHOOKS = require(webhooksDir)

const main = anime => new Promise(async (resolve, reject) => {
  if (!anime) {
    return reject(new Error('`anime` argument is missing in `discord-webhook`'))
  }

  Object.assign(anime, {
    image: `http://www.anime1.com/main/img/content/${anime.link}/${
      anime.link
    }-210.jpg`
  })

  for (const webhook of WEBHOOKS) {
    console.log(`Pushing to '${path.basename(webhook)}'.`)
    try {
      await request({
        method: 'POST',
        uri: webhook,
        headers: {
          'Content-Type': 'application/json'
        },
        json: {
          avatar_url: anime.image,
          username: anime.title.length >= 32
            ? `${anime.title.slice(0, 30)}..`
            : anime.title,
          embeds: [
            {
              color: anime.colour,
              title: `Episode ${anime.episode}`,
              url: `http://www.anime1.com/watch/${
                anime.link
              }/episode-${anime.episode}`,
              image: {
                url: anime.image
              },
              footer: {
                icon_url: 'http://i.imgur.com/EF3hc5B.png',
                text: `| ${moment().format('DD MM [\']YY, HH:mm:ss')}`
              }
            }
          ]
        }
      })
    } catch (e) {
      if (e instanceof StatusCodeError) return reject(e)
    }

    // NOTE: The purpose of this function is to prevent being
    // rate-limited therefore we'll wait 2.5 seconds before
    // each request.

    // eslint-disable-next-line
    await new Promise(res => {
      setTimeout(() => {
        res()
      }, 2500)
    })
  }

  resolve()
})

module.exports = main
