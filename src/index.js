const sqlite = require('sqlite')
const { default: scraper } = require('anime-scrape')
const path = require('path')
const dominant = require('./dominant-color')
const discordWebhook = require('./discord-webhook')

const dbDir = path.resolve(__dirname, '../releases.db')

const getDates = (...args) =>
  args.map(arg => {
    const tempDate = new Date(0)
    tempDate.setUTCSeconds(arg)
    return tempDate
  })

const main = async () => {
  const db = await sqlite.open(dbDir)

  const latestAnimes = await scraper.getLatest()

  await db.run(
    `
    CREATE TABLE IF NOT EXISTS anime_releases (
      id INTEGER
        CONSTRAINT anr_id_pk PRIMARY KEY
        AUTOINCREMENT,
      title VARCHAR(500) NOT NULL,
      link VARCHAR(500) NOT NULL,
      episode VARCHAR(50) NOT NULL,
      date DATETIME NOT NULL,
      updated NUMERIC(13, 13) NOT NULL,
      colour INTEGER NOT NULL
    );
  `.trim()
  )

  const animesToPush = []

  for (const anime of latestAnimes) {
    const exists = await db.get(
      'SELECT * FROM anime_releases WHERE link = ?;',
      [anime.link]
    )

    if (exists) {
      const [existsDate, animeDate] = getDates(exists.date, anime.date)
      if (
        existsDate.getTime() < animeDate.getTime() &&
        exists.episode !== anime.episode
      ) {
        await db.run(
          `
            UPDATE anime_releases
            SET episode = ?, date = ?, updated = ?
            WHERE link = ?;
          `.trim(),
          [anime.episode, anime.date, Date.now(), anime.link]
        )
      } else continue
    } else {
      console.log(`Discovered '${anime.title} Episode ${anime.episode}'.`)
      const colour = await dominant(anime)

      await db.run(
        `
        INSERT INTO anime_releases (title, link, episode, date, updated, colour)
        VALUES (?, ?, ?, ?, ?, ?);
        `.trim(),
        [anime.title, anime.link, anime.episode, anime.date, Date.now(), colour]
      )
    }

    const response = await db.get(
      'SELECT * FROM anime_releases WHERE link = ?;',
      anime.link
    )

    animesToPush.push(response)
  }

  if (!animesToPush.length) {
    return 0
  }

  for (const anime of animesToPush) {
    console.log(`NEW:'${anime.title} Episode ${anime.episode}'.`)
    await discordWebhook(anime)
  }
}

main()
